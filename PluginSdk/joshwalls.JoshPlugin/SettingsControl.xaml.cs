﻿using System.Windows.Controls;

namespace joshwalls.JoshPlugin
{
    /// <summary>
    /// Logique d'interaction pour SettingsControlDemo.xaml
    /// </summary>
    public partial class SettingsControl : UserControl
    {
        public JoshPlugin Plugin { get; }

        public SettingsControl()
        {
			InitializeComponent();
        }

        public SettingsControl(JoshPlugin plugin) : this()
        {
            this.Plugin = plugin;
        }


    }
}
