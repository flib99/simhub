﻿namespace joshwalls.JoshPlugin
{
    /// <summary>
    /// Settings class, make sure it can be correctly serialized using JSON.net
    /// </summary>
    public class JoshPluginSettings
    {
        public int SpeedWarningLevel = 100;
    }
}