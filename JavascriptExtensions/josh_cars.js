var cars = {
	"Aston Martin DBR9 GT1": {
		"class": "GT1",
		"shortClass": "GT1",
		"shortName": "DBR9"
	},
	"Audi 90 Quattro GTO": {
		"class": "GTO",
		"shortClass": "GTO",
		"shortName": "A90"
	},
	"Audi R18 2016": {
		"class": "LMP1",
		"shortClass": "P1",
		"shortName": "R18"
	},
	"Audi R8 LMS": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "R8"
	},
	"Audi RS 3 LMS": {
		"class": "TCR",
		"shortClass": "TCR",
		"shortName": "RS3"
	},
	"BMW M4 GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "M4"
	},
	"BMW M4 GT4": {
		"class": "GT4",
		"shortClass": "GT4",
		"shortName": "M4"
	},
	"BMW M8 GTE": {
		"class": "GTE",
		"shortClass": "GTE",
		"shortName": "M8"
	},
	"Cadillac CTS-V Racecar": {
		"class": "GTA",
		"shortClass": "GTA",
		"shortName": "CTS-V"
	},
	"Chevrolet Corvette C6R GT1": {
		"class": "GT1",
		"shortClass": "GT1",
		"shortName": "C6R"
	},
	"Chevrolet Corvette C7 Daytona Prototype": {
		"class": "DP",
		"shortClass": "DP",
		"shortName": "C7"
	},
	"Chevrolet Corvette C8.R": {
		"class": "GTE",
		"shortClass": "GTE",
		"shortName": "C8R"
	},
	"Dallara F312 F3": {
		"class": "F3",
		"shortClass": "F3",
		"shortName": "F3"
	},
	"Dallara IR01": {
		"class": "iR01",
		"shortClass": "iR01",
		"shortName": "iR01"
	},
	"Dallara IR18": {
		"class": "IndyCar",
		"shortClass": "Indy",
		"shortName": "IR18"
	},
	"Dallara P217 LMP2": {
		"class": "LMP2",
		"shortClass": "P2",
		"shortName": "P217"
	},
	"Ferrari 488 GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "488"
	},
	"Ferrari 488 GTE": {
		"class": "GTE",
		"shortClass": "GTE",
		"shortName": "488"
	},
	"Ford Fiesta RS WRC": {
		"class": "WRC",
		"shortClass": "WRC",
		"shortName": "Fiesta"
	},
	"Ford GT GT2": {
		"class": "GT2",
		"shortClass": "GT2",
		"shortName": "FGT"
	},
	"Ford GT GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "FGT"
	},
	"Ford GT GTE": {
		"class": "GTE",
		"shortClass": "GTE",
		"shortName": "FGTE"
	},
	"Ford Mustang FR500S": {
		"class": "Mustang",
		"shortClass": "Mustang",
		"shortName": "Mustang"
	},
	"Formula Renault 2.0": {
		"class": "FR2.0",
		"shortClass": "FR2.0",
		"shortName": "FR2.0"
	},
	"Formula Renault 3.5": {
		"class": "FR3.5",
		"shortClass": "FR3.5",
		"shortName": "FR3.5"
	},
	"Mazda MX-5 Cup": {
		"class": "MX-5 Cup",
		"shortClass": "MX5",
		"shortName": "MX5"
	},
	"HPD ARX-01c": {
		"class": "LMP2",
		"shortClass": "P2",
		"shortName": "HPD"
	},
	"Indy Pro 2000 PM-18": {
		"class": "Indy Pro",
		"shortClass": "IPro",
		"shortName": "PM18"
	},
	"Kia Optima": {
		"class": "Kia",
		"shortClass": "Kia",
		"shortName": "Kia"
	},
	"Lamborghini Huracan GT3 EVO": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "Lambo"
	},
	"Lotus 49": {
		"class": "F1",
		"shortClass": "F1",
		"shortName": "L49"
	},
	"Lotus 79": {
		"class": "F1",
		"shortClass": "F1",
		"shortName": "L79"
	},
	"McLaren 570S GT4": {
		"class": "GT4",
		"shortClass": "GT4",
		"shortName": "570S"
	},
	"McLaren MP4-12C GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "MP412C"
	},
	"McLaren MP4-30": {
		"class": "F1",
		"shortClass": "F1",
		"shortName": "MP430"
	},
	"Mercedes AMG GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "AMG"
	},
	"Nissan GTP ZX-T": {
		"class": "GTP",
		"shortClass": "GTP",
		"shortName": "ZXT"
	},
	"Pontiac Solstice": {
		"class": "Solstice",
		"shortClass": "Solstice",
		"shortName": "Solstice"
	},
	"Pontiac Solstice - Rookie": {
		"class": "Solstice",
		"shortClass": "Solstice",
		"shortName": "Solstice"
	},
	"Porsche 718 Cayman GT4": {
		"class": "GT4",
		"shortClass": "GT4",
		"shortName": "718"
	},
	"Porsche 911 GT3 Cup (991)": {
		"class": "Porsche Cup",
		"shortClass": "PCUP",
		"shortName": "PCup"
	},
	"Porsche 991 RSR": {
		"class": "GTE",
		"shortClass": "GTE",
		"shortName": "RSR"
	},
	"Porsche 919 2016": {
		"class": "LMP1",
		"shortClass": "P1",
		"shortName": "919"
	},
	"Radical SR8": {
		"class": "Radical",
		"shortClass": "Radical",
		"shortName": "SR8"
	},
	"Ruf RT 12R AWD": {
		"class": "Ruf",
		"shortClass": "Ruf",
		"shortName": "Ruf AWD"
	},
	"Ruf RT 12R C-Spec": {
		"class": "Ruf",
		"shortClass": "Ruf",
		"shortName": "Ruf C"
	},
	"Ruf RT 12R RWD": {
		"class": "Ruf",
		"shortClass": "Ruf",
		"shortName": "Ruf RWD"
	},
	"Ruf RT 12R Track": {
		"class": "Ruf",
		"shortClass": "Ruf",
		"shortName": "Ruf Track"
	},
	"SCCA Spec Racer Ford": {
		"class": "SRF",
		"shortClass": "SRF",
		"shortName": "SRF"
	},
	"Skip Barber Formula 2000": {
		"class": "Skippy",
		"shortClass": "Skippy",
		"shortName": "Skippy"
	},
	"Subaru WRX STI": {
		"class": "WRX",
		"shortClass": "WRX",
		"shortName": "Subaru"
	},
	"Supercars Ford Mustang GT": {
		"class": "Supercar",
		"shortClass": "SC",
		"shortName": "Mustang"
	},
	"Supercars Holden ZB Commodore": {
		"class": "Supercar",
		"shortClass": "SC",
		"shortName": "Holden"
	},
	"USF2000": {
		"class": "USF2000",
		"shortClass": "USF2000",
		"shortName": "USF2000"
	},
	"Volkswagen Beetle GRC": {
		"class": "WRX",
		"shortClass": "WRX",
		"shortName": "Beetle"
	},
	"VW Jetta TDI Cup": {
		"class": "Jetta",
		"shortClass": "Jetta",
		"shortName": "Jetta"
	},
	"Williams-Toyota FW31": {
		"class": "F1",
		"shortClass": "F1",
		"shortName": "FW31"
	},
	"BMW Z4 GT3": {
		"class": "GT3",
		"shortClass": "GT3",
		"shortName": "Z4"
	},
	"Dallara DW12": {
		"class": "IndyCar",
		"shortClass": "Indy",
		"shortName": "DW12"
	},
	"Dallara Indycar - circa 2011": {
		"class": "IndyCar",
		"shortClass": "Indy",
		"shortName": "Indy 09"
	},
	"Mazda MX-5 Roadster": {
		"class": "MX-5 Cup",
		"shortClass": "MX5",
		"shortName": "MX5"
	},
	"Pro Mazda": {
		"class": "Pro Mazda",
		"shortClass": "PM",
		"shortName": "PM"
	},
	"Riley Daytona Prototype": {
		"class": "DP",
		"shortClass": "DP",
		"shortName": "Riley"
	},
	"Ford Falcon FG01 V8": {
		"class": "Supercar",
		"shortClass": "SC",
		"shortName": "Falcon"
	},
	"Holden Commodore 2014": {
		"class": "Supercar",
		"shortClass": "SC",
		"shortName": "Holden VF"
	}
};