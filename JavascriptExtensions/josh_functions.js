const username = "Josh Walls";

var prt = {
	"Mazda MX-5 Cup": {
		"id": 1,
		"adjective": "",
		"name": "The Mazda",
		"assists": {
			"TCSlip": false,
			"TCGain": false,
			"TC": false,
			"BB": false,
			"ABS": true
		}
	},
	"Ferrari 488 GTE": {
		"id": 2,
		"adjective": "Feisty",
		"name": "Felicity",
		"assists": {
			"TCSlip": true,
			"TCGain": true,
			"TC": false,
			"BB": true,
			"ABS": false
		}
	},
	"Ferrari 488 GT3": {
		"id": 3,
		"adjective": "Horny",
		"name": "Harriet",
		"assists": {
			"TCSlip": false,
			"TCGain": false,
			"TC": true,
			"BB": true,
			"ABS": true
		}
	},
	"Dallara P217 LMP2": {
		"id": 4,
		"adjective": "Murderous",
		"name": "Maisie",
		"assists": {
			"TCSlip": true,
			"TCGain": true,
			"TC": false,
			"BB": true,
			"ABS": false
		}
	}
}

function assists(car) {
	if (prt[car]) {
		return prt[car].assists
	} else {
		return {
			"TCSlip": true,
			"TCGain": true,
			"TC": false,
			"BB": true,
			"ABS": false
		}
	}
}

function carName(car) {
	if (prt[car]) {
		return prt[car].adjective + ' ' + prt[car].name;
	} else {
		return car;
	}
}

function irating() {
	if ($prop('IRacingExtraProperties.iRacing_Player_iRating') != 0) {
		root['irating'] = $prop('IRacingExtraProperties.iRacing_Player_iRating');
	}
	return root['irating']
}

function color() {
	if (isnull($prop('IRacingExtraProperties.iRacing_Player_LicenceColor'))) {
		root['color'] = isnull($prop('IRacingExtraProperties.iRacing_Player_LicenceColor'),"h");
	}
	return root['color']
}

function safety() {
	if ($prop('IRacingExtraProperties.iRacing_Player_SafetyRating') != "R 0.01" || $prop('IRacingExtraProperties.iRacing_Player_SafetyRating') != "") {
		root['safety'] = $prop('IRacingExtraProperties.iRacing_Player_SafetyRating');
	}
	return root['safety']
}

var tracks = JSON.parse(readtextfile('C:\\Program Files (x86)\\Britton IT Ltd\\CrewChiefV4\\trackLandmarksData.json'));

function cornerName() {
	var track = $prop('TrackId');
	var percent = $prop('GameRawData.Telemetry.LapDist');
	var corner;
	var trackid;
	var trackdata;

	for (var i = 0; i < (tracks.TrackLandmarksData).length; i++) {
		if (tracks.TrackLandmarksData[i].irTrackName == track) {
			trackdata = tracks.TrackLandmarksData[i];
		}
	}

	trackdata.trackLandmarks.forEach(function(i, _) {
		if (percent >= i.distanceRoundLapStart && percent < i.distanceRoundLapEnd) {
			corner = i.landmarkName.replace(/_/g, ' ');

			words = corner.split(" ");

			for (var j = 0; j < words.length; j++) {
				words[j] = words[j][0].toUpperCase() + words[j].substr(1);
			}

			corner = words.join(" ");
		}
	})

	regex = /(\D+)(\d+)/

	return corner.replace(regex, "$1 $2");
}

function carClass(car) {
	return cars[car].class;
}

function shortCarClass(car) {
	return cars[car].shortClass;
}

function shortCarName(car) {
	return cars[car].shortName;
}

function sessionCPI() {
	var scpi =  (($prop('GameRawData.SessionData.WeekendInfo.TrackNumTurns') * $prop('CompletedLaps')) + ($prop('GameRawData.Telemetry.LapDistPct') * $prop('GameRawData.SessionData.WeekendInfo.TrackNumTurns'))) / $prop('GameRawData.SessionData.DriverInfo.DriverIncidentCount');
	if (scpi > cpi[499]) {
		return cpi[499]
	} else {
		return scpi
	}
}

function CPI() {
	var oldcpi = cpi[parseInt($prop('IRacingExtraProperties.iRacing_Player_SafetyRating').split(' ')[1] * 100)];

	return oldcpi;
}

function LPI() {
	return CPI() / $prop('GameRawData.SessionData.WeekendInfo.TrackNumTurns');
}